const request = require('request');

const config = {
  addressUrl: `http://localhost:3010/api/address-value`,
  addressTypeUrl: `http://localhost:3010/api/address-type`
};

let values;

const findRequest = (url, filter) => {
  let reqUrl = `${url}?filter=${encodeURIComponent(JSON.stringify(filter))}`;
  return getRequest(reqUrl);
};

const updateAddress = (id, data) => {
  return putRequest(config.addressUrl + '/' + id, data);
};

const removeAddress = (url) => {
  return new Promise((resolve, reject) => {
    request.delete(url, (error, response, body) => {
      if (error)
        return reject(error);
      if (typeof body === 'string')
        body = JSON.parse(body);
      if (body) {
        return resolve(body);
      }
      resolve();
    })
  });
};

const getRequest = (url) => {
  return new Promise((resolve, reject) => {
    request.get(url, {
      headers: {
        'Content-Type': 'application/json'
      }
    }, (error, response, body) => {
      if (error)
        return reject(error);
      if (typeof body === 'string')
        body = JSON.parse(body);
      if (body) {
        return resolve(body);
      }
      resolve();
    });
  });
};

const putRequest = (url, data) => {
  return new Promise((resolve, reject) => {
    return request.put({
      headers: {
        'Content-Type': 'application/json'
      },
      url: url,
      body: JSON.stringify(data)
    }, (error, response, body) => {
      if (error)
        return reject(error);
      if (typeof body === 'string')
        body = JSON.parse(body);
      if (body && body.data) {
        console.log(`address updated`, body.data.id);
        resolve(body.data);
      }
      resolve();
    });
  });
};

const getValues = (typeId, parents) => {
  return new Promise((resolve, reject) => {
    let url = `${config.addressUrl}/?filter=` + encodeURIComponent(`{
      "where": {
        "typeId":"${typeId}",
        "parents":"${parents}",
        "isDeleted": false
      } 
    }`);

    request.get(url, {
      headers: {
        'Content-Type': 'application/json'
      }
    }, (error, response, body) => {
      if (error)
        return reject(error);
      if (typeof body === 'string')
        body = JSON.parse(body);
      if (body && body.data && body.data.length > 0) {
        return resolve(body.data);
      }
      resolve();
    });
  })
};

const getValuesByNameParent = (name, typeId, parents) => {
  return new Promise((resolve, reject) => {
    let url = `${config.addressUrl}/?filter=` + encodeURIComponent(`{
      "where": {
        "name": "${name}",
        "typeId": "${typeId}",
        "parents": "${parents}",
        "isDeleted": false
      } 
    }`);

    request.get(url, {
      headers: {
        'Content-Type': 'application/json'
      }
    }, (error, response, body) => {
      if (error)
        return reject(error);
      if (typeof body === 'string')
        body = JSON.parse(body);
      if (body && body.data && body.data.length > 0) {
        return resolve(body.data);
      }
      resolve();
    });
  })
};

let first;
const processValues = (i, typeId, parentId) => {
  return new Promise((resolve, reject) => {
    if (values.length == 0) {
      console.log('no values found');
      return resolve();
    }
    console.log(`running ${i}/${values.length}`);

    let item = values[i];
    return new Promise((res, rej) => {
      return getValuesByNameParent(item.name, typeId, parentId)
        .then(result => {
          if(result && result.length > 0) {
            first = result[0];
            let filteredValues = result.filter(r => r.id != first.id);
            return Promise.all(filteredValues.map(v => {
              return findRequest(config.addressUrl, {
                where: {
                  parents: v.id,
                  isDeleted: false
                }
              }).then(address => {
                if(address && address.data && address.data.length > 0) {
                  return Promise.all(address.data.map(item => {
                    console.log('replacing parents: ', item.id)
                    item.parents = item.parents.map(parent => parent == v.id ? first.id : parent);

                    let addressId = item.id;
                    delete item.id;

                    return updateAddress(addressId, item);
                  }))
                }
                return Promise.resolve();
              }).then(() => {
                console.log('removing duplicate: ', v.id);
                return removeAddress(config.addressUrl + '/' + v.id);
              })
            }));
          }
          return Promise.resolve();
        })
        .then(() => {
          let district;
          let id = first.id;
          delete first.id;

          return Promise.all(first.parents.map(p => {
            return getRequest(config.addressUrl + '/' + p).then(parent => {
              if(parent && parent.data) {
                district = parent.data;

                if(district.typeId == '5975c25dcb091f0011a53122' || district.typeId == '5a3d39bcad4dac888fb903e9') {
                  first.parents = first.parents.filter(id => id != district.id);
                  
                  console.log('removing district from parents: ', id)
                  return updateAddress(id, first)
                }
              }
            })
          }))
        })
        .then(res)
        .catch(rej);
    }).then(() => {
      if(i >= values.length - 1)
        return resolve();
      processValues(i + 1, typeId, parentId).then(resolve);  
    })
  })
};

let typeStreet = '55c1f1d0d39acd07553e4fdd';
let typeAvenue = '5a005b89fab7b10011eee63d';
let cityId = '598aa6749bcf6300110ec97f';
getValues(typeStreet, cityId).then(addresses => {
  values = addresses;
  return processValues(0, typeStreet, cityId);
}).then(() => {
  console.log('done')
}).catch(err => {
  console.log('error')
  console.error(err)
})