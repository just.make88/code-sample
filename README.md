This script is to find duplicate streets with different types of districts(parents) in address-values collection,
and replace it's parents with first found address id, and remove all districts from parents in each street


// collection name address-value

```
addressValueModel = {
    _id: ObjectId(''),
    name: '',
    typeId: ObjectId(''), // foreign key of addressType id
    parents: [] // array ObjectId,
    isDeleted: false,
    metadata: {}
};
```


// collection name address-type

```
addressValueModel = {
    _id: ObjectId(''),
    name: '', // ex. street
    shortName: '' // st.
};
```
